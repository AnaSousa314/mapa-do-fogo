# Mapa do fogo

<!-- <img src="exemplo-image.png" alt="exemplo imagem"> -->

> O projeto tem como objetivo criar um mapa interativo estilo 'time line' de queimadas urbanas, utilizando para isso,informações providas pela população atravês de uma aplicação web com layout amigavel para dispostivos moveis.

Um modelo de prototipo está sendo desenhado no figma:

[Projeto no Figma](https://www.figma.com/file/tZvVgG2TFWplxVNOfJsZx8/Mapa-do-Fogo?node-id=207%3A275)

### Ajustes e melhorias

O projeto ainda está em desenvolvimento e as próximas atualizações serão voltadas para as seguintes tarefas:

- [ ] Template base;
- [ ] MapInput;
- [ ] Back-end para o suporte o Map Input;
- [ ] Map Time Line;
- [ ] Back-end para o prover dados para a visualização no Map Time Line;

## 💻 Pré-requisitos

Como projeto ainda esta na fase de concepção ainda não os temos definido, sinta-se a vontade para contribuir.

<!-- Antes de começar, verifique se você atendeu aos seguintes requisitos: -->
<!---Estes são apenas requisitos de exemplo. Adicionar, duplicar ou remover conforme necessário--->
<!-- * Você instalou a versão mais recente de `<linguagem / dependência / requeridos>`
* Você tem uma máquina `<Windows / Linux / Mac>`. Indique qual sistema operacional é compatível / não compatível.
* Você leu `<guia / link / documentação_relacionada_ao_projeto>`. -->

<!-- ## 🚀 Instalando <nome_do_projeto>

Para instalar o <nome_do_projeto>, siga estas etapas:

Linux e macOS:
```
<comando_de_instalação>
```

Windows:
```
<comando_de_instalação>
```

## ☕ Usando <nome_do_projeto>

Para usar <nome_do_projeto>, siga estas etapas:

```
<exemplo_de_uso>
```

Adicione comandos de execução e exemplos que você acha que os usuários acharão úteis. Fornece uma referência de opções para pontos de bônus! -->

## 📫 Contribuindo para <mapa-do-fogo>
<!---Se o seu README for longo ou se você tiver algum processo ou etapas específicas que deseja que os contribuidores sigam, considere a criação de um arquivo CONTRIBUTING.md separado--->
Para contribuir com <mapa-do-fogo>, siga estas etapas:

1. Bifurque este repositório.
2. Crie um branch: `git checkout -b <nome_branch>`.
3. Faça suas alterações e confirme-as: `git commit -m '<mensagem_commit>'`
4. Envie para o branch original: `git push origin <mapa-do-fogo> / <local>`
5. Crie a solicitação de pull.

Como alternativa, consulte a documentação do GitHub em [como criar uma solicitação pull](https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/creating-a-pull-request).

## 🤝 Colaboradores

Agradecemos às seguintes pessoas que contribuíram para este projeto:

<table>
  <tr>
    <td align="center">
      <a href="#">
        <img src="https://avatars.githubusercontent.com/u/6855092?v=4" width="100px;" alt="Foto do Thiago Almeida no GitHub"/><br>
        <sub>
          <b>Thiago Almeida</b>
        </sub>
      </a>
    </td>
  </tr>
</table>


<!-- ## 😄 Seja um dos contribuidores<br>

Quer fazer parte desse projeto? Clique [AQUI](CONTRIBUTING.md) e leia como contribuir. -->

## 📝 Licença

Esse projeto está sob licença. Veja o arquivo [LICENÇA](LICENSE.md) para mais detalhes.

[⬆ Voltar ao topo](#mapa-do-fogo)<br>
